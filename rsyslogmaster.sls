rsyslog:
  pkg:
    - installed

/etc/rsyslog.conf:
  file.managed:
    - source: salt://syslog/rsyslogmaster.conf
    - user: root
    - group: root
    - mode: 777