import-docker-key:
  cmd.run:
    - name: apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    - creates: /etc/apt/sources.list.d/docker.list
/etc/apt/sources.list.d/docker.list:
  file.managed:
    - source: salt://docker.list

docker:
  pkg.installed:
    - name: docker-engine
  service.running:
    - name: docker
    - require:
      - pkg: docker-engine

wordpress_app:  
  dockerng.running:
    - name: wordpress_app
    - image: repo/wordpress:latest
    - port_bindings: 80:80
    - environment:
      - WORDPRESS_DB_HOST: db:3306
      - WORDPRESS_DB_PASSWORD: wordpress
    - links: wordpress_db:mysql

wordpress_db:  
  dockerng.running:
    - name: wordpress_db
    - image: repo/mariadb:latest
    - ports: 3306/tcp    
    - binds: ./mysql:/var/lib/mysql:rw
    - environment:
      - MYSQL_ROOT_PASSWORD: wordpress
      - MYSQL_DATABASE: wordpress
      - MYSQL_USER: wordpress
      - MYSQL_PASSWORD: wordpress


# Credits to https://opsnotice.xyz/docker-with-saltstack/    