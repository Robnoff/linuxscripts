#!/bin/bash

echo "Looking for packages"

if dpkg --get-selections | grep --quiet  apache2; then
	echo "Apache2 exists, continuing..."
else
	echo "apache2 not found, installing..."
	sudo apt-get install -y apache2 apache2-utils
	echo "Apache2 install finished..."
fi

if /usr/sbin/apachetcl -M | grep -i cgi; then
	echo "Dynazoom installed, continuing ..."
else
	echo "Dynazoom not found, installing ..."
	sudo apt-get install -y libcgi-fast-perl libapache2-mod-fcgid
fi

echo "Packages installed, continuing ..."

echo "Installing Munin Master ..."

sudo apt-get install -y munin

cd /etc/munin

sudo sed -i -e 's+/var/cache/munin/www+/PATHHERE/g' munin.conf
