munin-node:
  pkg:
    - installed
/etc/munin/munin-node.conf:
  file.managed:
    - source: salt://munin/munin-node.conf
    - user: root
    - group: root
    - mode: 777
    - template: jinja
    - defaults:
      munin_master: "^10\\.0\\.0\\.7$"