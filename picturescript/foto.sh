#!/bin/bash

#declare variables
PAD=$1;
tijd=$2;


#search for the designated path
if [ ! -d "$PAD" ]; then
	echo "$PAD path not found";

else
	echo "$PAD found, copying pictures...";
	for f in "$PAD"/*
	do
		echo "$f"
	done
fi
